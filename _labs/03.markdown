---
layout: lab
title: Lab 3 - Queue Lab
subtitle: Looking at Pointers!
summary: |
    Here, we will use our expertise of pointers to implement a simple linked list data structure. 
    Since we're here, you will have to implement some defensive programming techniques! (check for your NULLs :)
released: 11:59 PM Wednesday, June 16th, 2021.
due: 11:59 PM Thursday, June 24th, 2021.
---
{% capture code_link %} {{site.url}}{{site.baseurl}}/labs/03/queuelab-handout.zip {% endcapture %}

**Note**: You must use **malloc** and not use calloc nor realloc in this assignment!

**Note**: Debugging is a bleach. Start early :)
{:.blinking}

# Overview

This lab will give you practice in the style of C programming you will
need to be able to do proficiently, especially for the later assignments
in the course (e.g., Cache Lab and Malloc Lab). Some of the skills
tested are:

-   Explicit memory management, as required in C.

-   Creating and manipulating pointer-based data structures.

-   Working with strings.

-   Enhancing the execution performance of key operations by implementing data structures
    that trade-off storage for execution speed.

-   Implementing robust code that handles error conditions and operates correctly with invalid
    arguments, including NULL pointers.

The lab involves implementing a deque, or double-ended queue, supporting
both last-in, first-out (LIFO) and first-in-first-out (FIFO) queueing
disciplines. The underlying data structure is a singly-linked list,
enhanced to make some of the operations more efficient.

# Logistics


**Start early enough to get it done before the due date.** Assume things
will not go according to plan, and so you must allow extra time for
heavily loaded systems, dropped Internet connections, corrupted files,
traffic delays, minor health problems, etc.

This is an *individual* project. All submissions are electronic using an
autotester service (Gradescope). While the assignment has been tested on
machines running several versions of Linux and OS X, the actual testing
for your code will be done using OS and compiler configurations similar
to those on the Thoth Linux machines. We advise you to test your code on
the Thoth Linux machine before submitting it.

**Always test your submission on Gradescope in advance.** We will not
return any points lost because you did the lab in a different
environment and have not tested it on Gradescope.

Before you begin, please take the time to **review the course policy on
academic integrity** at the course web site.

Remember that you are not allowed to search for help online, and you are
not allowed to ask other students for help, show them your code, or
discuss the specifics of the solution.

In particular, we remind you that referring to solutions from previous
quarters or from a similar course at another university or on the web is
**cheating**. We will run similarity-detection software over submitted
student programs, including programs from past quarters and online
repositories.

# Resources


Beyond the lecture material, which should help greatly, you may consult several online and offline resources.

1.  **C programming.** Our recommended text is Kernighan and Ritchie,
    The C Programming Language, second edition. Copies are on reserve in
    (e.g.) the Hillman Library. For this assignment, Chapters 5 and 6 are
    especially important. There are good online resources as well,
    including:
    [C Programming on wikibooks](https://en.wikibooks.org/wiki/C_Programming).

2.  **Linked lists.** You can consult the lecture materials of your
    previous CS 445 class (in Java!). For example:
    [Michael Lipschultz's lecture materials](http://people.cs.pitt.edu/~lipschultz/cs445/notes07.html)

3.  **Asymptotic (big-Oh) notation.** If you are unsure what "O(n)"
    means, check this out:
    [Cornell lecture notes on this subject.](https://www.cs.cornell.edu/courses/cs3110/2012sp/lectures/lec19-asymp/review.html)

4.  **C reference.** Searching the internet for function names will get you a long way. Often it will point you to the [CPP Reference website](https://en.cppreference.com/w/c). Some useful functions for this lab include:

    -   Memory management in this [memory guide](https://en.cppreference.com/w/c/memory):
        Functions malloc and free. Note you are not permitted to use
        calloc or any alternative memory allocation function.

    -   Strings library in this [strings guide](https://en.cppreference.com/w/c/string/byte):
        Functions strlen, strcpy, and strncpy. Beware of the behavior of
        strncpy when truncating strings!

**Teachers mistakes**: `strncpy` has strange behavior when truncating strings! It does not necessarily add a NULL terminator 😬.

As the Academic Integrity Policy states, you should not search the web
or ask others for solutions or advice on the specifics of the lab. That
means that search queries such as "linked-list implementation in C\" are
off limits.

# Downloading the assignment

Your lab materials are contained in an archive file called
`queuelab-handout.zip`, which you can download to your Linux machine as
follows.

      $ wget {{code_link}} -O queuelab-handout.zip

Start by copying queuelab-handout.zip to a protected directory in your
Thoth account machine in which you plan to do your work. Then give the
command below

        $ unzip queuelab-handout.zip

This will create a directory called queuelab-handout that contains a
number of files. Consult the file README for descriptions of the files.
You will modify only the files queue.h and queue.c.

**WARNING:** Do not let the Windows WinZip program open up your .zip
file (many Web browsers are set to do this automatically). Instead, save
the file to your Linux directory and use the Linux unzip program to
extract the files. In general, for this class you should NEVER use any
platform other than Linux to modify your files. Doing so can cause loss
of data (and important work!).

# Implementation

The file queue.h contains declarations of the following structures:
```
/* Linked list element */ typedef struct ELE {
    char *value; 
    struct ELE *next;
} list_ele_t;

/* Queue structure */ typedef struct {
    list_ele_t *head;   /* Linked list of elements */
} queue_t;
```

![**Linked-list implementation of a queue.** Each list element has a
value field, pointing to an array of characters (C's representation of
strings), and a next field pointing to the next list element. Characters
are encoded according to the ASCII encoding (shown in
hexadecimal.)

![Linked list image]({{site.baseurl}}/03/linked-list.png)

These data structures are combined to implement a queue of strings, as
illustrated in the Figure. The top-level representation of a queue is a
structure of type `queue_t`. In the starter code, this structure contains
only a single field `head`, but you will want to add other fields, *wink*. The
queue contents are represented as a singly-linked list, with each
element represented by a structure of type `list_ele_t`, having fields
value and next, storing a pointer to a string and a pointer to the next
list element, respectively. The final list element has its next pointer
set to `NULL`. You may add other fields to the structure `list_ele`,
although you need not do so.

Recall that a string is represented in C as an array of `char` values. In essentially every modern machine, a `char` is represented as a single byte. To store a string of length `l` the array must be at least `l + 1` in size (common mistakes TM). The first `l` elements storing the data of the string itself and the final element storing the null-terminator (a byte with the explicit value of 0.)

The `value` field of the `list_ele_t` is a pointer to such an array. The figure above shows the strings `cab`, `bead`, and `cab` represented as separate arrays. The values for each character come from the [ASCII encoding](asciitable.com) where letters a through e are represented as hexadecimal values `0x61` through `0x65`. Each list element should have its own unique copy of each string.

In our C code, a queue is a pointer of type `queue_t*`. We distinguish two special cases.
  - A `NULL` queue is one where the pointer has a value of `NULL`.
  - An empty queue is one where the queue pointer itself is valid, but the `head` field points to `NULL`.

Your code must deal with being passed a queue of either of these types along with, of course, a valid queue with one or more items.

# Programming Task


Your task is to modify the code in queue.h and queue.c to fully
implement the following functions.

-   `q_new`: Create a new, empty queue.
-   `q_free`: Free all storage used by a queue.
-   `q_insert_head`: Attempt to insert a new element at the head of the queue (LIFO).
-   `q_insert_tail`: Attempt to insert a new element at the tail of the queue (FIFO).
-   `q_remove_head`: Attempt to remove the element at the head of the queue.
-   `q_size`: Compute the number of elements in the queue.
-   `q_reverse`: Reorder the list so that the queue elements are reversed in order. This function must not use any of the other functions nor de-allocate/allocate nodes in the list! Instead, **it should rearrange the existing elements**.

More details can be found in the comments in file `queue.c`, including how to handle invalid operations (e.g., removing from an empty or NULL queue), and what side effects and return values the functions should have.

For functions that provide strings as arguments, you are expected to allocate and copy those strings to the list. You must use `malloc` to allocate the space and then copy the string to the newly allocated space. You may assume the string is well formed and contains the `NULL` character. So, you should not assume any fixed upper bound on the length of a string, just allocate space for each string based on its length (therefore: it's fine to use unsafe string functions).

Remember to include space for the terminating character!
{:.blinking}

When it comes time to free a list element, you are also expected to free the space used by the string. 

Two of the functions: `q_insert_tail` and `q_size` will require their implementation to adhere to some performance requirements. Constant time access: `O(1)`, i.e. the function run-time does not depend on the size of the list!
Therefore, their implementation *may* (**will**) require some changes to the `queue_t` structure, as the information currently stored there will only allow naive implementations that scale linearly with the size of the queue: `O(n)`, where `n` is the number of elements in the queue.

You can do this by including other fields
in the queue\_t data structure and managing these values properly as
list elements are inserted, removed and reversed.

Your program will be tested on queues with over `1 Megaelements`.
You will find that you cannot operate on such long lists using recursive
functions, since that would require too much stack space. I believe most linuxes (linuces? linuxi?) only give you a maximum of 8MiB. Instead, you always need to use loops when you want to traverse the elements in a list.

# Testing

You can compile your code using the command:

```
make
```

If there are no errors, the compiler will generate an executable program
qtest, providing a command interface with which you can create, modify,
and examine queues. Documentation on the available commands can be found
by starting this program and running the help command:
```
./qtest 
cmd> help
```

The following file (traces/trace-eg.cmd) illustrates an example command
sequence:

```
# Demonstration of queue testing framework # Initial queue is NULL.
show
# Create empty queue new
# Fill it with some values.     First at the head ih dolphin
ih bear ih gerbil
# Now at the tail it meerkat
it bear
# Reverse it reverse
# See how long it is size
# Delete queue. Goes back to a NULL queue. free
# Exit program quit
```

You can see the effect of these commands by operating qtest in batch
mode:

```
./qtest -f traces/trace-eg.cmd
```

With the starter code, you will see that many of these operations are not implemented properly.

The traces directory contains $15$ trace files, with names of the form `trace-k-cat.txt`, where k is the trace number, and cat specifies the category of properties being tested. Each trace consists of a sequence of commands, similar to those shown above. They test different aspects of the correctness, robustness, and performance of your program. You can use these, your own trace files, and direct interactions with qtest to test and debug your program.

# Evaluation

Your program will be evaluated using the fifteen traces described above. You will be given credit (either $6$ or $7$ points, depending on the trace) for each one that executes correctly, summing to a maximum score of $100$. This will be your score for the assignment --- the grading is completely automated.

The driver program driver.py runs qtest on the traces and computes the score. This is the same program that will be used to compute your score with Gradescope. You can invoke the driver with the following command in the terminal: `make test`.

# Submission

You need to upload both your `queue.c` (your C Implementation) and `queue.h` (your C Header) files (and **only those files!**) to Gradescope, which will autotest your submission and record your scores. You may submit as often as you like until the due date.

# Remember

It is good to start this assignment early. C, like many things in life, takes practice. This assignment stresses the importance of careful and deliberate programming. Write short statements, do less on each line, and comment your code very liberally (no matter what the YouTube Java Stars (is this a thing?) tell you about self-documenting code!).

If you find yourself frustrated, take an hour away from it. Come back to it with a fresh point of view. Read the code over and what it does. Remember that the computer is going to do exactly what it is told, so do not read back what you think you wrote. Read what is actually there and if that is what you intended.

It may help to draw out a diagram (like the animations we saw in class!) of the data structure as you trace your own code. You might find yourself, as I did, going,

> “Ah, right, I can’t assign `cur = cur->next` after I have done `cur->next = last`! I need to keep that pointer in a temp variable somewhere.”

Mastery does not mean you always get it right on the first try!