---
layout: lab
title: Lab 4 - Assembly Lab
subtitle: Bombs and exploits!
summary: |
    Let's play bomb squad! I'll post the rest later
released: 11:59 PM Wednesday, June 35th (or so), 2021.
due: 11:59 PM Thursday, July 8th, 2021.
extended: 11:59 PM Sunday, July 11th, 2021.
---

{% capture code_link %} {{site.url}}{{site.baseurl}}/labs/04/assemblylab-handout.zip {% endcapture %}

**Warning**: The setup for this lab is a bit janky. So let me know if any of the moving parts fails (you cannot access the website for the program, etc.)

# Introduction
In this lab, you will gain basic familiarity with x86-64 assembly instructions and how they are used to implement typical C language features, including comparison, loops, etc. The point of this lab is to get a basic understanding of how a computer navigates through a program’s execution at a fundamental level. Once a program is compiled into a binary, understanding what it does becomes a challenge. Since a great way to understand how something works is to understand how to break it, you’ll be hacking assembly programs.

By doing this lab, you will also gain experience using the `gdb` debugger to step through assembly code and other tools such as `objdump`. You will also get a better understanding of the stack and parameter-passing mechanisms of x86-64 machine code and will understand how buffer overflow exploits can affect a program. Roughly speaking, a buffer overflow occurs when a program writes to memory beyond a buffer/array addresses and clobbers nearby data that was not supposed to be changed by the write. 

You will learn about the runtime operation of programs and understand the nature of some security weaknesses so that you can avoid them when you write system code.


# Logistics

As usual, this is an individual project. All handins are electronic via Gradescope. Before you begin, please take the time to review the course policy on academic integrity at the course web site. Remember that:
- You are not allowed to search for help online!
- You are not allowed to ask other students for help, show them your code, or discuss the specifics of the solution.

IMPORTANT: We strongly advise you to develop and run your code on the Thoth Linux machine and to test
your submission on Gradescope. We will not return any points lost because you did the lab in a different
environment than Thoth and have not tested it on Gradescope before the deadline!

# Handouts

## Bomb defusing handouts
Your lab materials for the first half of the lab can be obtained through the following link: [Download lab materials](https://occam.dev:449).

In that page, fill in the form with your Pitt ID and email: e.g.: lun8 and lun8@pitt.edu. You will get a file in return :)
That file will be generated uniquely for you, don't share it with anyone!

To extract the files in the archive, run the following command in Linux:

```
tar xf <filename>.tar
```

This will create a directory that contains some files.

## Buffer overflow handouts
{:.blinking}

The materials for the second half can be found in an archive file called assemblylab-handout.zip, which you can
download to your Linux machine as follows.
```
wget {{code_link}} -O assemblylab-handout.zip
```

This will download and copy assemblylab-handout.zip to your directory on the Thoth
machine in which you will do your work. Then give the command
```
unzip assemblylab-handout.zip
```

# Defusing Traps

Navigate to the directory you extracted. In this part of the lab you will be working with a binary program `bomb`. Your task is figure out the correct input you need to give to the program in order to defuse the bomb. However, there’s a catch: you will only have access to the binary executables, not the source code.

To defuse these traps, you will use the objdump utility (a program used to display information about object files and executables like the assembly code), and GDB.
You may also find it helpful to look through this [x86-64 instruction reference](https://web.stanford.edu/class/cs107/resources/x86-64-reference.pdf). For example, it is important to know that, by the x86 64-bit calling convention, the first six arguments are passed in the registers %rdi, %rsi, %rdx, %rcx, %r8, and %r9 (in that order).

The program is composed of 4 traps, each with increasing difficulty. Check the main file you received in the archive, it may help you getting started.

**Note**: In the class of June 24th, I gave some info in class on how to analyse a binary. Go check it!

## Submitting your answers

When you solve each of the traps, place the codes in a text file named `bomb.txt`. The answer to each trap in its own line. E.g.:
```
This is my answer
123 123 32 32
Blarg foo bar
askdj
<make sure to include an empty line>
```

To make sure the file is correctly formatted, run the `bomb` program dumping the file contents into it, like so:
```
./bomb < bomb.txt
```

The `<` symbol redirects the contents of the file into the program's standard input. So it's as if you are typing the contents of the file.

If this step doesn't work, it's likely the Gradescope's auto-testing will fail :(. So let me know if you have any trouble with  this.


# Exploiting memory

In this part of the lab you will get to the play the role of a malicious user, and hack a program we give you.
Follow the instructions on the "Buffer overflow handouts" section above.

Your exploit strings will attack the binary program TARGET. This program is set up in a way that the
stack positions will be consistent from one run to the next and so that data on the stack can be treated as
executable code. These features make the program vulnerable to attacks where the exploit strings contain
the byte encodings of executable code.

In the real world, there are protections developed to prevent this type of attack. But in this lab, we removed them >:]


## Buffer Overflow Vulnerability
As you know from lecture, C gives you as the programmer so much power to access memory, thus it is very
easy to modify data you were not directly intending to modify. What happens if you accidentally give a
malicious user the unchecked power to modify your program’s data?

In C, we often preallocate memory (as a buffer) for a well defined purpose, such as reading user input. A
buffer is a reserved sequence of memory addresses for reading and writing data. When the program writes
more data to the buffer than the buffer has space for, it will overwrite data outside the buffer. This is called
a **buffer overflow**!

There is a function, now deprecated, in the C library called `gets`. This function is notoriously unsafe
because it is vulnerable to buffer overflow attacks. The `gets` function was designed to be a convenient
utility for reading a string from _stdin_. This is implemented by simply reading bytes (characters) into a
buffer until the newline character (`\n`) is found. Well what if the BUFFER OVERFLOWS? Ha!

Consider the following C program:

```
#include <stdio.h>
int main(int argc, char **argv) {
    char buffer[8];
    gets(buffer);
    return 0;
}
```

The function `gets()` has no way to determine whether the destination buffers are large enough to store
the string the user inputs. It simply copies sequences of bytes, possibly overrunning the bounds of the storage
allocated at the destinations (aka the **stack**! contents. Where unimportant stuff is located. You know stuff like 
the return address, stuff like saved registers, and stuff like local variables. Nothing of importance really).

Specifically, the program shown above reads a string from _stdin_ to an 8 byte **stack allocated** buffer. This
seems fine at first glance, but consider how this program could break given how gets is implemented.
What happens if the user inputs more than 7 characters before they press enter? 
(**YES**, 7! Did you already forget about the NULL terminator? :)

In fact, `gets` will null-terminate the input string. If the user were to input 5 characters (e.g. “hello”)
and then hit enter, it would place 6 characters ['h', 'e', 'l', 'l', 'o', '\0`] into memory.

## Attack 1

Your mission, if you choose to accept it **\***, is to explore a buffer overflow to change the value of a constant, stack allocated
variable. Function `gets` is called within TARGET by a function read_my_number having the
following C code:

**\* Disclaimer: if you don't accept it you get zero in this part :(**

```
void read_my_number() {
    // stack allocate some variables
    const int my_number = 72; // my number is a constant so it can’t change
    char buf[BUFSIZE];
    // print favorite number
    printf("My number is %d and nothing can change that\n", my_number);
    // get a string from stdin
    gets(buf);
    // print favorite number again bc its a great number
    printf("My number is %d and nothing can change that\n", my_number);
}
```

This simple program prints the programmers favorite number (twice), stored in `my_number`, and makes
a call to gets with a 32 byte buffer (given by `#define BUFSIZE 32`). You will notice that passing an
input of 32 characters or less works fine and has a consistent and sane output (for example, using input
`1234567` as below):

```
$ ./target
My number is 72 and it will always be 72 and nothing can change that
1234567 # user input
My number is 72 and it will always be 72 and nothing can change that
Returned to main safe and sound
$
```

We know, however, that the buffer is only 32 bytes and writing more than 32 characters will overflow into other stack
memory. (yikes) So we can be evil little imps and try to exploit the vulnerability :)

But how do we know how much to write to get to the stack memory associated with the variable
`my_number`? Well, to understand how a buffer overflow occurs, it is very useful to visualize what is happening
on the stack. Look at the assembly code, and decode what is happening to the stack (drawing helps!).

Your task is to be clever with the strings you feed the `TARGET` so that it does more interesting things. These
are called exploit strings. Importantly, for this first attack, you need to give an input that will cause a buffer
overflow to change `my_number` to the number `449` at runtime. But that is only the first step, as you must also 
**ensure the program does not segfault**. I.e., it should return to the caller and leave orderly.


Note that your machine (Thoth) uses a 64 bit architecture with **little-endian** data representation. That means
that if we have a 4-byte integer in memory, it will be stored with the least significant bytes first. Keep this
in mind when writing to memory using your exploit string!

Because we are providing a string to `TARGET`, the program is encoding the characters we input into `ASCII`
character codes for each byte. This means that to write specific numeric byte values to memory, we would
have to map our characters to bytes with an ASCII table.

It is worth mentioning that your exploit strings will typically contain byte values that do not correspond to
the ASCII values for printing characters. The program `HEX2RAW` will enable you to generate these raw
strings. See `Appendix A` below for more information on how to use `HEX2RAW`.

**Important points:**
- Your exploit string must not contain byte value `0x0a` at any intermediate position, since this is the
ASCII code for newline (`‘\n’`). When gets encounters this byte, it will assume you intended to
terminate the string.
- `HEX2RAW` expects two-digit hex values separated by one or more white spaces. So if you want to
create a byte with a hex value of 0, you need to write it as 00. To create the word 0xdeadbeef
you should pass “ef be ad de” to `HEX2RAW` (note the reversal required for little-endian byte
ordering).
- Don't forget the `\0`: when things segfault and you don't understand why, remember `\0`

Your exploit string should be written in the exploit1.txt file. If you want to ensure that your exploit
string works, you can run:
```
cat exploit1.txt | ./hex2raw | ./target
```

**Note:** Check the appendices below for information on how to run this in GDB should you need it.

After running this command, you need to see in the terminal the second `printf` displaying 

```My number is 449 and nothing can change that```

Without causing segfault!


## Attack 2

In this second attack, you will hack the call stack of the `TARGET` program. Your task here is to redirect
the program at runtime to execute an existing procedure. By disassembling `TARGET`, you may have already
noticed that we have also written and compiled a function called hack. You will do this, of course, without
putting a direct invocation in the program code/binary. You will create an exploit string to cause a buffer
overflow that results in the execution of the hack function rather than returning the execution to main.
**Again, this should not cause the program to segfault.**

Once you have your exploit working, you will write the contents of your exploit into file `exploit2.txt`.
To verify if it is working correctly, you can run:

```
./hex2raw < exploit2.txt | ./target
```

This command should produce the following message in the terminal: 
```
You’ve been HACKED!
```

Followed with a strange message about an argument..... hmmm.....

**Some Advice:**
- Consider targeting the retq instruction to load a particular address into `%rip`. From where does `retq` take a value? If you don’t know the answer to this question, review the lecture material and textbook on the x86-64 call stack.
- All the information you need to devise your exploit string for this attack can be determined by examining
a disassembled version of `TARGET`. Use `objdump -d` to get this dissembled version.
- The idea is to position a byte representation of the starting address for hack so that the `ret` instruction
at the end of the code for `read_my_number` will transfer control to hack.
- Be careful about byte ordering :).
- You might want to use `GDB` to step the program through the last few instructions of `read_my_number`
to make sure it is doing the right thing. Inside `GDB`, it may be useful to look at the contents of the
stack, using a command such as: `x /10gx $rsp` or `x /80bx $rsp`.
- The placement of buf within the stack frame for gets depends on the value of compile-time constant
`BUFSIZE`, as well the allocation strategy used by GCC. You will need to examine the disassembled
code to determine its position.

# Attack 3

This is the interesting one :)

This part involves injecting a small amount of code as part of your exploit string. Within the file target
there is code for function hack having the following C representation:

```
void hack(unsigned val) {
    printf("You’ve been HACKED!\n");
    if (val == COOKIE) {
        printf("Yes! Called passing the right argument (%d)\n", val);
    } else {
        printf("Misfire! Called it passing the wrong argument (%d)\n", val);
    }
    exit(0);
}
```

Your task is still to get `TARGET` to execute the code for hack rather than returning to main. In this case,
however, you must make it appear to hack as if you have passed the correct argument required, which is
given by the `COOKIE` macro/constant that you need to figure out what it is by looking at the assembly
code.

**Some Advice:**
- You will want to position a byte representation of the address of your injected code in such a way that
`ret` instruction at the end of the code for `read_my_number` will transfer control to it.
- Recall that the first argument to a function is passed in register `%rdi`.
- Your injected code should set the register to the value `COOKIE`, and then use a `ret` instruction to
transfer control to the first instruction in hack.
- Do not attempt to use `jmp` or `call` instructions in your exploit code. The encodings of destination
addresses for these instructions are difficult to formulate. Use `ret` instructions for all transfers of
control, even when you are not returning from a call.
- See the discussion in `Appendix B` on how to use tools to generate the byte-level representations of
instruction sequences.

# Assembly and gdb
The GNU debugger, gdb, is a command line debugger tool available on virtually every platform. You
can trace through a program line by line, examine memory and registers, look at both the source code and
assembly code (note we are not giving you the source code), set breakpoints, set memory watch points, and
write scripts.

The debugger has many features that allow you to trace and debug code at the assembly level. You can print
the value in a register by prefixing its name with $ or use the command info reg to dump the values of
all registers:

```
(gdb) p $rsp
(gdb) info reg
```

The disassemble command will print the disassembly for a function by name. The x command supports
an i format which interprets the contents of a memory address as an encoded instruction.

```
(gdb) disassemble main // disassemble, print all instructions of main
(gdb) x/8i main // disassemble, print first 8 instructions
```

You can set a breakpoint at a particular assembly instruction by its direct address or offset within a
function
```
(gdb) b *0x08048375
(gdb) b *main+7 // break at instruction 7 bytes into main
```

You can advance by instruction (instead of source line) using the stepi and nexti commands.

```
(gdb) stepi
(gdb) nexti
```

Good luck and have fun!


# Submission
To receive credits for the lab, you need to upload the following files to Gradescope:

- Bomb defusing (5+6+7+7 points)
  - bomb (your executable!)
  - bomb.txt (the answers to the traps) 
- Exploiting memory (25 points)
  - exploit1.txt (5 points)
  - exploit2.txt (10 points)
  - exploit3.txt (10 points)



# Appendix A - Using HEX2RAW

`HEX2RAW` takes as input a hex-formatted string. In this format, each byte value is represented by two hex
digits. For example, the string `"012345"` could be entered in hex format as `30 31 32 33 34 35 00`. 
(Recall that the ASCII code for decimal digit x is `0x3x`, and that the end of a string is indicated by a
null byte.)

The hex characters you pass to `HEX2RAW` should be separated by whitespace (blanks or newlines). We
recommend separating different parts of your exploit string with newlines while you’re working on it.
`HEX2RAW` supports C-style block comments, so you can mark off sections of your exploit string. For
example:

```
48 c7 c1 f0 11 40 00 /* mov $0x40011f0,%rcx */
```

Be sure to leave space around both the starting and ending comment strings (`"/*"`, `"*/"`), so that the
comments will be properly ignored.

If you generate a hex-formatted exploit string in the file `exploit.txt`, you can apply the raw string to
`TARGET` in several different ways:

1. You can set up a series of pipes to pass the string through `HEX2RAW`.

```
unix> cat exploit.txt | ./hex2raw | ./target
```

2. You can store the raw string in a file and use I/O redirection:

```
unix> ./hex2raw < exploit.txt > exploit-raw.txt
unix> ./target < exploit-raw.txt
```

This approach can also be used when running from within GDB:

```
unix> gdb target
(gdb) run < exploit-raw.txt
```

# Appendix B - Generating Byte Codes

Using GCC as an assembler and `OBJDUMP` as a disassembler makes it convenient to generate the byte codes
for instruction sequences. For example, suppose you write a file `example.s` containing the following
assembly code:

```
# Example of hand-generated assembly code

pushq      $0xabcdef   # Push value onto stack
addq       $17,%rax    # Add 17 to %rax
movl       %eax,%edx   # Copy lower 32 bits to %edx
```

The code can contain a mixture of instructions and data. Anything to the right of a `‘#’` character is a
comment.

You can now assemble and disassemble this file:

```
unix> gcc -c example.s
unix> objdump -d example.o > example.d
```

The generated file example.d contains the following:
```
example.o:     file format elf64-x86-64
Disassembly of section .text:

0000000000000000 <.text>:
  0: 68 ef cd ab 00                pushq $0xabcdef
  5: 48 83 c0 11                   add $0x11,%rax
  9: 89 c2                         mov %eax,%edx
```

The lines at the bottom show the machine code generated from the assembly language instructions. Each
line has a hexadecimal number on the left indicating the instruction’s starting address (starting with 0), while
the hex digits after the ‘:’ character indicate the byte codes for the instruction. Thus, we can see that the
instruction `push $0xABCDEF` has hex-formatted byte code `68 ef cd ab 00`.

From this file, you can get the byte sequence for the code:
```
68 ef cd ab 00 48 83 c0 11 89 c2
```

This string can then be passed through `HEX2RAW` to generate an input string for the `TARGET` program.
Alternatively, you can edit example.d to omit extraneous values and to contain C-style comments for
readability, yielding:

```
68 ef cd ab 00 /* pushq $0xabcdef */
48 83 c0 11 /* add $0x11,%rax */
89 c2 /* mov %eax,%edx */
```

This is also a valid input you can pass through `HEX2RAW` before sending to the `TARGET` program. 