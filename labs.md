---
layout: page
title: Labs
permalink: /labs/
---

## Academic Integrity Statement

All assignment submissions must be the sole work of each individual student. Students may not read or copy another student's solutions or share their own solutions with other students. Students may not review solutions from students who have taken the course in previous years. Submissions that are substantively similar will be considered cheating by all students involved, and as such, students must be mindful not to post their code publicly. Any use of electronics or other resources during an examination will be considered cheating. 

If you have any doubts about whether a particular action may be construed as cheating, ask the instructor for clarification before you do it. The instructor will make the final determination of what is considered cheating.

Cheating in this course will result in a standard penalty of a failing grade and may be subject to further disciplinary action. Lesser penalties may occur, depending on the circumstances. The penalty will always be worse than if you had not turned in the assignment at all.

Be aware that you may be asked to explain your code to a member of our course staff using only what you have submitted: your comments in the code should be such that you can determine what your code does and why a few weeks later, if needed.

Remember that:
  - You will work on all assignments by yourself.
  - Assignments will be auto-graded and must be submitted online using Gradescope
  - All labs are due 11:59pm on the shown due date.
  - The late penalty is 10% per day.
  - Each student has 5 late days for the semester.
  - You can submit any assignment at most 3 days late.
  - For each assignment, there is a limit on the number of grace days that can be applied, as is indicated in the table below.
  - You may resubmit your work until the deadline, with your most recent submission counting for credit.
  - We highly recommend making partial submissions as you go and not waiting until the last (literal) minute to submit. 
  - We will not return any points lost because you did the lab in a different environment and have not tested it on Gradescope before the deadline.
  - You should know that fairly sophisticated plagiarism detection software will be used on the programming assignments.

# Lab grade distribution

|Lab| Out | Weight | Max late days | Name | Due |
|L0 | 27th of May | 1% | 0 | Hello Lab | 3rd of June |
|L1 | 1st of June | 6% | 1 | Data Lab | 10th of June |
|L2 | 8th of June | 6% | 1 | Pointer Lab | 17th of June |
|L3 | 15th of June | 7% | 2 | Queue Lab | 24th of June |
|L4 | 22th of June | 8% | 2 | Assembly Lab | 8st of July |
|L5 | 6th of July | 12% | 3 | Malloc Lab | 22th of July |
|L6 | 20th of July | 10% | 2 | Cache Lab | 6th of August |

{% for lab in site.labs %}

{% if lab.solution %}

**Solution**: [{{ lab.title }} Solution]({{site.baseurl }}{{ lab.url }})

{% else %}
# [{{ lab.title }}: {{ lab.subtitle }}]({{ site.baseurl }}{{ lab.url }})

**Released**: {{ lab.released }}

**Due**: {{ lab.due }}

{{ lab.summary }}
{% endif %}

{% endfor %}
